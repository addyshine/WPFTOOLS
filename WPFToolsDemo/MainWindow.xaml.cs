﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace WPFToolsDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StackPanel_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Button btn = (Button)e.OriginalSource;

                Type type = this.GetType();
                Assembly assembly = type.Assembly;
                Window win = (Window)assembly.CreateInstance(type.Namespace + "." + btn.Content.ToString());
                if (win != null)
                {
                win.Show();

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_BootstrapStyle(object sender, RoutedEventArgs e)
        {
            (new Windows.BootstrapDemo()).Show();
        }
    }
}
