﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTools.Controls
{
    /// <summary>
    /// 用于显示图标
    /// </summary>
    public class IconTextBlock : Control
    {
        static IconTextBlock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(IconTextBlock), new FrameworkPropertyMetadata(typeof(IconTextBlock)));
        }


        /// <summary>
        /// 图标位置
        /// </summary>
        public Dock IconDock
        {
            get { return (Dock)GetValue(IconDockProperty); }
            set { SetValue(IconDockProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconDock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconDockProperty =
            DependencyProperty.Register("IconDock", typeof(Dock), typeof(IconTextBlock), new PropertyMetadata(Dock.Left));

      

        /// <summary>
        /// 文本内容
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(IconTextBlock), new PropertyMetadata(null));


        /// <summary>
        /// IconFont 图标
        /// </summary>
        public string IconFont
        {
            get { return (string)GetValue(IconFontProperty); }
            set { SetValue(IconFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFontProperty =
            DependencyProperty.Register("IconFont", typeof(string), typeof(IconTextBlock), new PropertyMetadata(null));


        /// <summary>
        /// FontAwesome 图标
        /// </summary>
        public string FontAwesome
        {
            get { return (string)GetValue(FontAwesomeProperty); }
            set { SetValue(FontAwesomeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontAwesome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontAwesomeProperty =
            DependencyProperty.Register("FontAwesome", typeof(string), typeof(IconTextBlock), new PropertyMetadata(null));



        /// <summary>
        /// 图标大小
        /// </summary>
        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(IconTextBlock), new PropertyMetadata(6.0));



        /// <summary>
        /// 图标颜色
        /// </summary>
        public Brush  IconForeground
        {
            get { return (Brush )GetValue(IconForegroundProperty); }
            set { SetValue(IconForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconForegroundProperty =
            DependencyProperty.Register("IconForeground", typeof(Brush ), typeof(IconTextBlock), new PropertyMetadata(new SolidColorBrush()));


        /// <summary>
        /// xaml路径图标
        /// </summary>
        public Geometry IconGeometry
        {
            get { return (Geometry)GetValue(IconGeometryProperty); }
            set { SetValue(IconGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconGeometry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconGeometryProperty =
            DependencyProperty.Register("IconGeometry", typeof(Geometry), typeof(IconTextBlock), new PropertyMetadata(null));



       

        
        
    }
}
