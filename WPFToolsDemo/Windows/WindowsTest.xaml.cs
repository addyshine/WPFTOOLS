﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFToolsDemo.Windows;
using WPFTools.Dialogs;

namespace WPFToolsDemo
{
    /// <summary>
    /// Windows.xaml 的交互逻辑
    /// </summary>
    public partial class WindowsTest 
    {
        public WindowsTest()
        {
            InitializeComponent();
            cboResizeMode.ItemsSource = Enum.GetValues(typeof(ResizeMode));
            ResizeModeSelect = ResizeMode.CanResizeWithGrip;
            IsHeaderVisible1 = true;

            //messagedialog
            cboMessageBoxButton.ItemsSource = Enum.GetValues(typeof(MessageBoxButton));
            this.DataContext = this;

        }



        public ResizeMode ResizeModeSelect { get; set; }

        public bool IsHeaderVisible1 { get; set; }

        private void btnBaseWindow_Click(object sender, RoutedEventArgs e)
        {
            BaseWindow b = new BaseWindow();

            b.IsHeaderVisible = IsHeaderVisible1;
            b.Title = "Title";
            b.ResizeMode = ResizeModeSelect;
            b.Show();
        }



        public string MessageDialogReturn
        {
            get { return (string)GetValue(MessageDialogReturnProperty); }
            set { SetValue(MessageDialogReturnProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MessageDialogReturn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageDialogReturnProperty =
            DependencyProperty.Register("MessageDialogReturn", typeof(string), typeof(WindowsTest), new PropertyMetadata("None"));

        
        public MessageBoxButton SelectedMessageBoxButton { get; set; }
        public MessageBoxImage sss { get; set; }
        public MessageBoxOptions sss1 { get; set; }

        private void btnMessageDialog_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageDialog.ShowDialog("请输入结果哈哈哈哈hhh哈哈哈哈哈哈hhh哈哈哈哈哈哈hhh哈哈哈哈哈哈hhh哈哈哈哈哈哈哈哈哈hhh哈哈哈哈哈哈哈哈哈hhh哈哈哈哈哈哈", "标题", SelectedMessageBoxButton, this);
            //WPFTools.PackIconModern.Data = WPFTools.PackIconModernKind._3dObj;      //.PackIconMaterialKind 
            //WPFTools.PackIconModern icon = new WPFTools.PackIconModern();
            //icon.Kind = WPFTools.PackIconModernKind._3dX;
            //MessageDialogReturn = icon.Data;

            MessageDialogReturn = result.ToString();
        }

        private void btnWaitingBox_Click(object sender, RoutedEventArgs e)
        {
            WaitingBox.Show(this,
                new Action(
                    () =>
                    {
                        System.Threading.Thread.Sleep(3000);
                    }
                    )
                , "load......");
        }

        private WaitingBox box;
        private void btnWaitingBox1_Click(object sender, RoutedEventArgs e)
        {
            box = new WaitingBox(
                new Action(
                    () =>
                    {
                        for (int i = 0; i < 100;i++ )
                        {
                            box.ChangeText("已经加载"+i.ToString()+"%");
                            System.Threading.Thread.Sleep(50);
                        }
                    }));

            box.Owner = this;
            box.Text = "测试";
           box.ShowDialog();

        }

        private void btnSplashBox_Click(object sender, RoutedEventArgs e)
        {
            SplashBox box = new SplashBox();
            box.Title = "初始化";
            var re = box.ShowDialog();
            MessageBox.Show(re.ToString());
        }

        private void about_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WPFTools.Dialogs.AboutDialog dlg = new WPFTools.Dialogs.AboutDialog(this);

                //var dlg = new AboutDialog(this);
                //dlg.Title = "About the application";
                //dlg.UpdateStatus = "The application is updated.";
                //dlg.Image = new BitmapImage(new Uri(@"pack://application:,,,/DialogDemos;component/pt.png"));

                //// var uri = "http://opensource.linux-mirror.org/trademarks/opensource/web/opensource-400x345.png";
                //// dlg.Image = new BitmapImage(new Uri(uri, UriKind.Absolute));
                dlg.ShowDialog();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        
    }
}
