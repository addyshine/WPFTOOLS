﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace WPFTools.Controls
{
    /// <summary>
    /// 自定义文本框
    /// </summary>
    [TemplatePart(Name = "PART_ContentHost", Type = typeof(ScrollViewer))]
    [TemplatePart(Name = "PART_Button", Type = typeof(Button))]
    public class TextBoxEx : TextBox
    {
        //引用模板元素
        private const string PART_Button = "PART_Button"; //主要部分
        private const string PART_ContentHost = "PART_ContentHost"; //主要部分
        private Button button;
        private ScrollViewer scrollViewer;

        static TextBoxEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextBoxEx), new FrameworkPropertyMetadata(typeof(TextBoxEx)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            scrollViewer = GetTemplateChild(PART_ContentHost) as ScrollViewer;
            button = GetTemplateChild(PART_Button) as Button;
            button.Click += button_Click;
        }
        /// <summary>
        /// 按钮单击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void button_Click(object sender, RoutedEventArgs e)
        {
            //如果有清空按钮则清空 数据
            if (IsClearButton)
            {
                this.Text = null;
            }
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            // base.OnGotFocus(e);
            scrollViewer.Focus();
        }



        public static readonly DependencyProperty CornerRadiusProperty =
               DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(TextBoxEx), new PropertyMetadata(new CornerRadius(0)));
        /// <summary>
        /// 圆角大小,左上，右上，右下，左下
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }



        public string WaterMark
        {
            get { return (string)GetValue(WaterMarkProperty); }
            set { SetValue(WaterMarkProperty, value); }
        }

        

        /// <summary>
        /// 水印内容 
        /// </summary>
        public static readonly DependencyProperty WaterMarkProperty =
            DependencyProperty.Register("WaterMark", typeof(string), typeof(TextBoxEx), new PropertyMetadata(""));


        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

       /// <summary>
       /// Label标签的内容
       /// </summary>
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(TextBoxEx), new PropertyMetadata(""));

        public double LabelWidth
        {
            get { return (double)GetValue(LabelWidthProperty); }
            set { SetValue(LabelWidthProperty, value); }
        }

        /// <summary>
        /// Label标签的宽度
        /// </summary>
        public static readonly DependencyProperty LabelWidthProperty =
            DependencyProperty.Register("LabelWidth", typeof(double), typeof(TextBoxEx), new PropertyMetadata(double.NaN));



        public Thickness LabelMargin
        {
            get { return (Thickness)GetValue(LabelMarginProperty); }
            set { SetValue(LabelMarginProperty, value); }
        }

        /// <summary>
        ///  Label 的 Margin 属性
        /// </summary>
        public static readonly DependencyProperty LabelMarginProperty =
            DependencyProperty.Register("LabelMargin", typeof(Thickness), typeof(TextBoxEx), new PropertyMetadata(new Thickness(5,2,5,2)));


        public HorizontalAlignment LabelHorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(LabelHorizontalAlignmentProperty); }
            set { SetValue(LabelHorizontalAlignmentProperty, value); }
        }

        /// <summary>
        /// Label 的 HorizontalAlignment 属性
        /// </summary>
        public static readonly DependencyProperty LabelHorizontalAlignmentProperty =
            DependencyProperty.Register("LabelHorizontalAlignment", typeof(HorizontalAlignment), typeof(TextBoxEx), new PropertyMetadata(HorizontalAlignment.Right));




        #region 图标按钮部分

        /// <summary>
        /// IconFont 图标
        /// </summary>
        public string IconFont
        {
            get { return (string)GetValue(IconFontProperty); }
            set { SetValue(IconFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFontProperty =
            DependencyProperty.Register("IconFont", typeof(string), typeof(TextBoxEx), new PropertyMetadata(null));


        /// <summary>
        /// FontAwesome 图标
        /// </summary>
        public string FontAwesome
        {
            get { return (string)GetValue(FontAwesomeProperty); }
            set { SetValue(FontAwesomeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontAwesome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontAwesomeProperty =
            DependencyProperty.Register("FontAwesome", typeof(string), typeof(TextBoxEx), new PropertyMetadata(null));



        /// <summary>
        /// 图标大小
        /// </summary>
        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(TextBoxEx), new PropertyMetadata(6.0));


        /// <summary>
        /// xaml路径图标
        /// </summary>
        public Geometry IconGeometry
        {
            get { return (Geometry)GetValue(IconGeometryProperty); }
            set { SetValue(IconGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconGeometry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconGeometryProperty =
            DependencyProperty.Register("IconGeometry", typeof(Geometry), typeof(TextBoxEx), new PropertyMetadata(null));


        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(TextBoxEx));





        /// <summary>
        /// 是否为清空按钮
        /// </summary>
        public bool IsClearButton
        {
            get { return (bool)GetValue(IsClearButtonProperty); }
            set { SetValue(IsClearButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsClearButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsClearButtonProperty =
            DependencyProperty.Register("IsClearButton", typeof(bool), typeof(TextBoxEx), new PropertyMetadata(false));

        

        
        
        #endregion
        

        

    }
}
