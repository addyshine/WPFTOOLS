# WPFTOOLS

WPF的控件库 包装常用功能及UI控件

## NuGet

* 搜索 CC.WPFTools
* [NuGet地址](https://www.nuget.org/packages/CC.WPFTools/)
* PM> `Install-Package CC.WPFTools`

## 帮助文档

* <https://www.yuque.com/chch/wpftools>
* <https://gitee.com/chenhome/DOC_WPFTOOLS>

## 一个例子

* 主要实现了一个桌面小工具的功能
* <https://gitee.com/chenhome/OfficeHelper>

## 快速使用方法

在新建的WPF程序的 App.xaml 添加如下资源 则使用默认样式

```xml
<Application.Resources>
    <ResourceDictionary>
        <ResourceDictionary.MergedDictionaries>
            <ResourceDictionary Source="/WPFTools;component/Styles/UI.xaml"></ResourceDictionary>
            <ResourceDictionary Source="/WPFTools;component/Styles/UI.Light.xaml"></ResourceDictionary>
            <ResourceDictionary Source="/WPFTools;component/Resources/I18N/I18N_en-US.xaml" />
        </ResourceDictionary.MergedDictionaries>
    </ResourceDictionary>
</Application.Resources>
```

* `UI.xaml` 基础样式
* `UI.Light.xaml` 主题样式
* `I18N_en-US.xaml` 控件文字默认语言设置 默认为中文

## 控件预览

![](https://imchenchao.com/gitbook/wpftolls/_book/assets/windows_light.jpg)

![](https://imchenchao.com/gitbook/wpftolls/_book/assets/windows_dark.jpg)

![](https://imchenchao.com/gitbook/wpftolls/_book/assets/iconbutton_light.jpg)

![](https://imchenchao.com/gitbook/wpftolls/_book/assets/radiobuttonStyle_dark.jpg)

![](https://imchenchao.com/gitbook/wpftolls/_book/assets/datagrid_light.jpg)