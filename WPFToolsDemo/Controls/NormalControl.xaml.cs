﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFTools.Presentation;

namespace WPFToolsDemo
{
    /// <summary>
    /// NormalControl.xaml 的交互逻辑
    /// </summary>
    public partial class NormalControl : Window
    {
        List<Student> list;
        public NormalControl()
        {
            InitializeComponent();
             list = new List<Student>();

            for (int i = 0; i < 10; i++)
            {
                Student stu = new Student();
                stu.Name = "Name:" + i;
                stu.Class = "A";
                stu.Index = i+100000;
                stu.No = i;
                stu.Sex = true;
                if (i%2==0)
                {
                    stu.Sex = false;

                }
                list.Add(stu);
            }
            Console.WriteLine(list.Count);
            grid.ItemsSource = list;
            this.DataContext = this;
        }



        private MenuItem CreateSubMenu(string header)
        {
            var item = new MenuItem { Header = header };
            item.Items.Add(new MenuItem { Header = "Item 1" });
            item.Items.Add("Item 2");
            item.Items.Add(new Separator());
            item.Items.Add("Item 3");
            return item;
        }

        private void ShowContextMenu_Click(object sender, RoutedEventArgs e)
        {
            var contextMenu = new ContextMenu();

            contextMenu.Items.Add(new MenuItem { Header = "Item" });
            contextMenu.Items.Add(new MenuItem { Header = "Item with gesture", InputGestureText = "Ctrl+C" });
            contextMenu.Items.Add(new MenuItem { Header = "Item, disabled", IsEnabled = false });
            contextMenu.Items.Add(new MenuItem { Header = "Item, checked", IsChecked = true });
            contextMenu.Items.Add(new MenuItem { Header = "Item, checked and disabled", IsChecked = true, IsEnabled = false });
            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(CreateSubMenu("Item with submenu"));

            var menu = CreateSubMenu("Item with submenu, disabled");
            menu.IsEnabled = false;
            contextMenu.Items.Add(menu);

            contextMenu.IsOpen = true;
        }



        class Student
        {
            public int No { get; set; }
            public int Index { get; set; }
            public String  Name { get; set; }
            public bool Sex { get; set; }
            public String  Class { get; set; }

        }


    }
}
